@extends('layouts.main')

@section('content')

    <div class="flex items-center min-h-screen bg-gray-50 dark:bg-gray-900">
        <div class="container mx-auto">
            <div class="max-w-md mx-auto my-10 bg-white p-5 rounded-md shadow-sm">
                <div class="text-center">
                    <h1 class="my-3 text-3xl font-semibold text-gray-700 dark:text-gray-200">Update Item</h1>
                </div>
                <div class="m-7">
                    <form method="POST" enctype="multipart/form-data" href="/edit_menu/create_item/{id}/edit }}">
                        {{-- <form method="POST" enctype="multipart/form-data" href="{{ route('create_category', '[id => $post->id]' }}"> --}}
                        @method('put')
                        @csrf
                        <div class="mb-6">
                                
                            <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Item Name</label>
                            <input type="text" name="name" id="name" value="{{ $item->name }}" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                            @error('name') 
                            <div class="p-2">
                                <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                                  <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                                  <span class="inline-flex px-2">{{ $message}}</span>
                                </div>
                              </div>
                                @enderror
                        </div>
                        <div class="mb-6">
                            <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Price</label>
                            <input type="text" name="price" id="price" value=" {{ $item->price }}" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                            @error('price') 
                            <div class="p-2">
                                <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                                  <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                                  <span class="inline-flex px-2">{{ $message}}</span>
                                </div>
                              </div>
                                @enderror
                        </div>
                        <div class="mb-6">
                            <label for="category_id" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Category</label>
                            <select name="category_id" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @error('category_id') 
                            <div class="p-2">
                                <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                                  <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                                  <span class="inline-flex px-2">{{ $message}}</span>
                                </div>
                              </div>
                                @enderror
                        </div>

                        <div class="mb-6">
                            <button type="submit" class="w-full px-3 py-4 text-white bg-green-300 rounded-md hover:bg-green-600 focus:outline-none">Update</button>
                        </div>
                        <div class="mb-6">
                            <button class="w-full px-3 py-4 text-white bg-green-300 rounded-md hover:bg-green-600 focus:outline-none">Return Back</button>
                        </div>
                        <p class="text-base text-center text-gray-400" id="result">
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
