@extends('layouts.main')

@section('content')

    <div class="flex items-center min-h-screen bg-gray-50 dark:bg-gray-900">
        <div class="container mx-auto">
            <div class="max-w-md mx-auto my-10 bg-white p-5 rounded-md shadow-sm">
                <div class="text-center">
                    <h1 class="my-3 text-3xl font-semibold text-gray-700 dark:text-gray-200">Add a New Item</h1>
                </div>
                <div class="m-7">
                    <form method="POST" enctype="multipart/form-data" href="{{ route('save_item') }}">
                        @csrf
                        <div class="mb-6">
                            <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Item Name</label>
                            <input type="text" name="name" id="name" placeholder="Category Name" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                        </div>
                        <div class="mb-6">
                        <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Price</label>
                        <input type="text" name="price" id="price" placeholder="Enter Price" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                </div>
                        <div class="mb-6">
                            <label for="category_id" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Category</label>
                            <select name="category_id" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500">
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>



                        <div class="mb-6">
                            <button type="submit" class="w-full px-3 py-4 text-white bg-green-300 rounded-md hover:bg-green-600 focus:outline-none">Submit</button>
                        </div>
                        <div class="mb-6">
                            <a href="{{ route('edit_menu')}}" class="w-full px-3 py-4 text-white bg-green-300 rounded-md hover:bg-green-600 focus:outline-none">Return Back</a>
                        </div>
                        <p class="text-base text-center text-gray-400" id="result">
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
