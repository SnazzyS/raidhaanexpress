@extends('layouts.main')

@section('content')

<div id="wrapper" class="max-w-xl px-4 py-4 mx-auto">
    <div class="sm:grid sm:h-32 sm:grid-flow-row sm:gap-4 sm:grid-cols-3 bg-gray-200 p-3">
        <div class="flex flex-col justify-center px-4 py-4 bg-white border border-gray-300 rounded">
            <div>
                <div>

                </div>
                <p class="text-3xl font-semibold text-center text-gray-800">43</p>
                <p class="text-lg text-center text-gray-500">Pending</p>
            </div>
        </div>

        <div class="flex flex-col justify-center px-4 py-4 mt-4 bg-white border border-gray-300 rounded sm:mt-0">
            <div>
                <div>

                </div>
                <p class="text-3xl font-semibold text-center text-gray-800">43</p>
                <p class="text-lg text-center text-gray-500">Processing</p>
            </div>
        </div>

        <div class="flex flex-col justify-center px-4 py-4 mt-4 bg-white border border-gray-300 rounded sm:mt-0">
            <div>
                <div>

                </div>
                <p class="text-3xl font-semibold text-center text-gray-800">43</p>
                <p class="text-lg text-center text-gray-500">Total Delivered</p>
            </div>
        </div>
    </div>
</div>


<div class="flex flex-col mx-20	">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                    <tr>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            No.
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Address
                        </th>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Order Item
                        </th>

                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Order Total
                        </th>
                        <th scope="col" class="relative px-6 py-3">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>

                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach($customers as $customer)

                        <tr>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="flex items-center">

                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900">

                                        </div>

                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="text-sm text-gray-900">{{ $customer->address }} - {{ $customer->phone_number }}</div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                @foreach($customer->items as $item)
                                    <li>{{ $item->name }} - MVR {{ $item->price }} x {{ $item->pivot->quantity }} </li>
                                @endforeach
                                <div class="text-sm text-gray-900"> </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                @foreach($customer->items as $item)
                                <div class="text-sm text-gray-900">
                                @php
                                    $total += $item->pivot->quantity * $item->price; 
                                @endphp

                                         {{ $item->pivot->quantity * $item->price }}
                                     </div>
                                @endforeach
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a href="" class="px-2 py-2 text-black-600 hover:text-green-white rounded-full hover:bg-yellow-300">Edit</a>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a href="#" class="px-2 py-2 text-black-600 hover:text-green-white rounded-full hover:bg-red-300">Delete</a>
                            </td>

                        </tr>
                    @endforeach
                    <!-- More people... -->
                    </tbody>
                </table>
            </div>
  @endsection
