<div class="flex items-center min-h-screen bg-gray-50 dark:bg-gray-900">
    <div class="container mx-auto">
        <div class="max-w-md mx-auto my-10 bg-white p-5 rounded-md shadow-sm">
            <div class="text-center">
                <h1 class="my-3 text-3xl font-semibold text-gray-700 dark:text-gray-200">Create New Order</h1>
            </div>
            <div class="m-7">
                <form method="POST" enctype="multipart/form-data" href="{{ route('save_order') }}">
                    @csrf
                    <div class="mb-6">
                        <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Phone Number</label>
                        <input type="text" name="phone" placeholder="Phone Number" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                        @error('phone')
                        <div class="p-2">
                            <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                                <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                                <span class="inline-flex px-2">{{ $message}}</span>
                            </div>
                        </div>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Address</label>
                        <input type="text" name="address" placeholder="Enter Address" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                    </div>
                    @error('address')
                    <div class="p-2">
                        <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                            <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                            <span class="inline-flex px-2">{{ $message}}</span>
                        </div>
                    </div>
                    @enderror

                    <div class="mb-6">
                        <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Road</label>
                        <input type="text" name="road" id="price" placeholder="Enter Road" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                    </div>
                    @error('road')
                    <div class="p-2">
                        <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                            <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                            <span class="inline-flex px-2">{{ $message}}</span>
                        </div>
                    </div>
                    @enderror
                    <div class="mb-6">
                        <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Landmark</label>
                        <input type="text" name="landmark" id="price" placeholder="Any famous places?" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                    </div>
                    @error('landmark')
                    <div class="p-2">
                        <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                            <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                            <span class="inline-flex px-2">{{ $message}}</span>
                        </div>
                    </div>
                    @enderror

                    <div class="mb-6">
                        <label for="category_id" name="city" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">City</label>
                        <select name="city" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500">
                            <option value="Male">Male</option>
                            <option value="Hulhumale">Hulhumale</option>
                        </select>
                    </div>
                    @error('city')
                    <div class="p-2">
                        <div class="inline-flex items-center bg-white leading-none text-pink-600 rounded-full p-2 shadow text-teal text-sm">
                            <span class="inline-flex bg-red-600 text-white rounded-full h-6 px-3 justify-center items-center">Error</span>
                            <span class="inline-flex px-2">{{ $message}}</span>
                        </div>
                    </div>
                    @enderror

                    {{--                            Item selecting part --}}
                    <div>
                        <div>
                            <label for="category_id" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Select Order Items</label>
                        </div>

                        <div class="card-body">
                            <table class="table" id="products_table">
                                <thead>
                                <tr>
                                    <th class="mb-2 text-sm text-gray-600 dark:text-gray-400">Product</th>
                                    <th class="mb-2 text-sm text-gray-600 dark:text-gray-400">Quantity</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orderProducts as $index => $orderProduct)
                                    <tr>
                                        <td >
                                            <select name="orderProducts[{{$index}}][product_id]"
                                                    wire:model="orderProducts.{{$index}}.product_id"
                                                    class="form-control">
                                                <option value="">-- choose product --</option>
                                                @foreach ($allProducts as $product)
                                                    <option value="{{ $product->id }}">
                                                        {{ $product->name }} (MVR{{ number_format($product->price, 2) }})
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number"
                                                   name="orderProducts[{{$index}}][quantity]"
                                                   class="form-control"
                                                   wire:model="orderProducts.{{$index}}.quantity" />
                                        </td>
                                        <td>
                                            <a class="bg-red-500 text-white px-6 py-2 rounded font-medium mx-3 hover:bg-red-600 transition duration-200 each-in-out" href="#" wire:click.prevent="removeProduct({{$index}})">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="row">
                                <div >
                                    <button class=" bg-green-300 text-white px-6 py-2 rounded font-medium mx-3 hover:bg-green-600 transition duration-200 each-in-out m-5"
                                            wire:click.prevent="addProduct">+ Add Another Product</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="mb-6">
                        <button type="submit" class="w-full px-3 py-4 text-white bg-green-300 rounded-md hover:bg-green-600 focus:outline-none">Submit</button>
                    </div>
                    <div class="mb-6">
                        <a href="{{ route('dashboard')}}" class="w-full px-3 py-4 text-white bg-green-300 rounded-md hover:bg-green-600 focus:outline-none">Return Back</a>
                    </div>
                    <p class="text-base text-center text-gray-400" id="result">
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
