<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index(){

        $categories = Category::all();

        return view ('item.create', compact('categories'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:items',
            'price' => 'required|integer|max:5000',
            'category_id' => 'required|integer',


        ]);

        Item::create([
            'name' => $request->name,
            'price' => $request->price,
            'category_id' => $request->category_id,

        ]);

        return redirect()->route('edit_menu')->with('message', 'Item Created Successfully');

    }

    public function edit($id){
        $item = Item::findOrFail($id);
        $categories = Category::all();

        return view('item.edit', compact('item', 'categories'));
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|between:0,5000.99',
            'category_id' => 'required|integer',
        ]);


        $item = Item::findOrFail($id);
        $item->name = $request->name;
        $item->price = $request->price;
        $item->category_id = $request->category_id;
        $item->save();

        return redirect()->route('edit_menu')->with('message', "Item Updated Successfully");
    }
}
