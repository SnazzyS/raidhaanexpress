<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $customers = Customer::with('items')->get();

        return view('dashboard', compact('customers'));
    }
}
