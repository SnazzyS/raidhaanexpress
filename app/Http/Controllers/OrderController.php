<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Item;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        $items = Item::all();

        return view('order.create', compact('items'));
    }

    public function store(Request $request){


//        $this->validate($request, [
//            'name' => 'required|unique:items',
//            'address' => 'required|max:5000',
//            'road' => 'string|nullable',
//            'landmark' => 'nullable',
//            'city' => 'required',
//        ]);


//         Customer::create([
//             'phone_number' => $request->phone,
//            'address' => $request->address,
//            'road' => $request->road,
//            'landmark' => $request->landmark,
//            'city' => $request->city
//
//         ]);


            $customer = Customer::create([
                'phone_number' => $request->phone,
                'address' => $request->address,
                'road' => $request->road,
                'landmark' => $request->landmark,
                'city' => $request->city,

            ]);

        foreach ($request->orderProducts as $product) {
                 $customer->items()->attach($product['product_id'],
                     ['quantity' => $product['quantity']]);
             }


        return 'Order stored successfully!';

    }
}
