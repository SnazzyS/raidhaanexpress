<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        return view('category.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:items',
            'price' => 'required|integer|max:5000',
            'category_id' => 'required|integer',


        ]);

        Item::create([
            'name' => $request->name,
            'price' => $request->price,
            'category_id' => $request->category_id,

        ]);



        return redirect()->route('edit_menu')->with('message', 'Category Created Successfully');
    }
}
