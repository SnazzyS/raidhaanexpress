<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index(){

        $items = Item::all();

        return view('edit_menu', compact('items'));
    }
}
