<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth'])->name('dashboard');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/edit_menu', [MenuController::class, 'index'])->name('edit_menu');


// Adding Categories
Route::get('/edit_menu/create_category', [CategoryController::class, 'index'])->name('create_category');
Route::post('/edit_menu/create_category', [CategoryController::class, 'store'])->name('save_category');

// adding items
Route::get('/edit_menu/create_item', [ItemController::class, 'index'])->name('create_item');
Route::post('/edit_menu/create_item', [ItemController::class, 'store'])->name('save_item');
Route::get('/edit_menu/create_item/{id}', [ItemController::class, 'edit'])->name('edit_item');
Route::put('/edit_menu/create_item/{id}', [ItemController::class, 'update'])->name('update_item');

// create order
Route::get('/create_order', [OrderController::class, 'index'])->name('create_order');
Route::post('/create_order', [OrderController::class, 'store'])->name('save_order');

